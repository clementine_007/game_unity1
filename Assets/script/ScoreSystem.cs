using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreSystem : MonoBehaviour
{
    public static int Score;
    public static int highScore;

    public Text scoreText;
    public Text totalscoreText;
    public Text highScoreText;

    //public static int scoreValue = 0;
    //catatan doang = score = math.clamp(score-10,0,9999)

    public void AddScore()
    {
        Score++;
        //scoreText.text = "Score: " + Score;
    }

    void Start()
    {
        highScore = PlayerPrefs.GetInt("Highscore");
    }


    void Update()
    {
        scoreText.text = "Score : " + Score.ToString();
        totalscoreText.text = "Score : " + Score.ToString();
        highScoreText.text = "HighScore : " + highScore.ToString();

        if (Score > highScore)
        {
            PlayerPrefs.SetInt("Highscore", Score);
        }

        highScore = PlayerPrefs.GetInt("Highscore");

    }
}
