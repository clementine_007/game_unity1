using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WordManager : MonoBehaviour
{
    public List<Word> words;

    public WordSpawner wordSpawner;

    //gameover
    public static bool isGameOver;
    public GameObject gameOverScreen;
    //score
    public GameObject score;
    //word
    private bool hasActiveWord;
    private Word activeWord;



    public void AddWord ()
    {
        if (!isGameOver)
        {
            Word word = new Word(WordGenerator.GetRandomWord(), wordSpawner.SpawnWord());
            Debug.Log(word.word);

            words.Add(word);
        }
        
    }

    public void TypeLetter (char letter)
    {
        if (!GameplayManager.isPauseGame)
        {
            if (hasActiveWord)
            {
                if (activeWord.GetNextLetter() == letter)
                {
                    activeWord.TypeLetter();
                }
                else
                {
                    hasActiveWord = false;
                }
            }
            else
            {
                foreach (Word word in words)
                {
                    if (word.GetNextLetter() == letter)
                    {
                        activeWord = word;
                        hasActiveWord = true;
                        word.TypeLetter();
                        break;
                    }
                }
            }
            if (hasActiveWord && activeWord.WordTyped())
            {
                RemoveWordx();
            }
        }
        
        
    }

    public void RemoveWordx()
    {
        FindObjectOfType<SoundEffects>().SfxDuar();
        hasActiveWord = false;
        words.Remove(activeWord);
    }

    //game over function

    private void Awake()
    {
        Time.timeScale = 1f;
        isGameOver = false;
        //isPauseGame = false;
        //Score.scoreValue = 0;
        ScoreSystem.Score = 0;
        Level.levelGame = 1;
        GameplayManager.totalSkill = 3;
        GameplayManager.nyawa = 3;
    }

    void Update()
    {
        
        if (isGameOver)
        {
            FindObjectOfType<SoundEffects>().LoseMusic();
            Destroy(GameObject.FindWithTag("kata"));
            Destroy(GameObject.FindWithTag("ui2"));
            Destroy(GameObject.FindWithTag("button_ui"));
            score.SetActive(false);
            gameOverScreen.SetActive(true);
            
        }
    }

    public void ReplayLevel()
    {
        SceneManager.LoadScene("Gameplay");
    }

    public void BackMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }


}
