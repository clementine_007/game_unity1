using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordGenerator : MonoBehaviour
{
    private static string[] wordList = { "miga","melon","grill","art","boy","vtuber","sleep","monkey",
                                        "cahkartun", "cow", "dragon", "train", "minum", "males", "video",
                                        "gatos", "cats", "moose", "mouse", "tikus", "rickroll", "unity",
                                        "empty", "qwerty" , "hot", "cold", "death" , "doggy", "girl",
                                        "orangecat", "bluecat", "faster", "black", "blue", "rotten",
                                        "eminem", "milk", "cofee","dinner","dash" , "rumble", "attack",
                                         "road", "get", "pizza","breakfast", "typing", "zombie", "rahul",
                                         "iron", "wakanda", "youtube", "wolf", "rentalpacar", "lazy",
                                         "crazy", "hatred", "suicide", "badman" , "batman", "food", "foot"};

    public static string GetRandomWord ()
    {
        int randomIndex = Random.Range(0, wordList.Length);
        string randomWord = wordList[randomIndex];

        return randomWord;
    }

}
