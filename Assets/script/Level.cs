using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level : MonoBehaviour
{
    public static int levelGame = 0;
    public static float aSpeed = 0.3f;
    Text levelText;

    public void LevelUp()
    {
        //levelGame++;
        //Level.levelGame = Mathf.Clamp(Level.levelGame + 1, 0, 5);
    }

    void Start()
    {
        levelText = GetComponent<Text>();
    }

    void Update()
    {
        levelText.text = "Level : " + levelGame;
    }
}
