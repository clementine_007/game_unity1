using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void play()
    {
        //Debug.Log("menu");
        SceneManager.LoadScene("Gameplay");
    }

    public void quit()
    {
        Debug.Log("keluar");
        Application.Quit();
    }
}
