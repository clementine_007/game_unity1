using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordDisplay : MonoBehaviour
{
    public Transform target;

    public Text text;
    public float fallSpeed = 0.01f;
    //pin fungsinya buat ngasih angka tiap kata untuk dikasih score per kata
    public static int pin;

    public blowTest blow;

    public void SetWord(string word)
    {
        text.text = word;
    }

    public void RemoveLetter()
    {
        FindObjectOfType<SoundEffects>().SfxDuar2();
        pin++;
        text.text = text.text.Remove(0, 1);
        text.color = Color.yellow;
        //FindObjectOfType<fire>().summonPelor();
        //ScoreSystem.Score += 1;
    }


    public void RemoveWord()
    {
        ScoreSystem.Score += pin;

        //if (ScoreSystem.Score > 0 && ScoreSystem.Score % 50 == 0)
        //{
        //    Level.levelGame = Mathf.Clamp(Level.levelGame + 1, 0, 5);
        //}

        if (ScoreSystem.Score >= GameplayManager.targetScore)
        {
            Level.levelGame = Mathf.Clamp(Level.levelGame + 1, 0, 5);
            GameplayManager.targetScore += 50;
        }

        pin = 0;
        Destroy(gameObject);
    }

    public void coba()
    {
        Destroy(gameObject);

        //while (!gameObject.activeInHierarchy)
        //{
          //  Destroy(gameObject);
       // }
    }

     void Start()
    {
        target = GameObject.FindWithTag("Player").GetComponent<Transform>();
    }

    private void Update()
    {
        if (!GameplayManager.isPauseGame)
        {
            //transform.Translate(0f, -fallSpeed * Time.deltaTime, 0f);
            transform.position = Vector2.MoveTowards(transform.position, target.position, fallSpeed + Level.levelGame * Time.deltaTime);
        } 
    }
}
