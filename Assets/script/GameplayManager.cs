using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameplayManager : MonoBehaviour
{
    //nyawa
    public static int nyawa;
    public Text nyawaText;
    //pause
    public static bool isPauseGame = false;
    public GameObject pauseScreen;
    //skill
    public static int totalSkill = 0;
    public Text skillText;
    //serialize icon
    [SerializeField] Image PauseIcon;
    [SerializeField] Image ResumeIcon;
    //
    public static int targetScore = 50;
    public Text targetText;

    void Start()
    {
        //Screen.SetResolution(1920, 1080, true);
        PauseIcon.enabled = true;
        ResumeIcon.enabled = false;
        FindObjectOfType<SoundEffects>().BgmMusic();
        isPauseGame = false;
        //lose = GetComponent<AudioSource>();
        //GetComponent<AudioSource>().clip = BGMs;
    }


    public void Update()
    {
        targetText.text = "as : " + targetScore.ToString();
        nyawaText.text = "Life : " + nyawa.ToString();
        skillText.text = "Skill : " + totalSkill.ToString();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPauseGame)
            {
                Resume();
            }else
            {
                Paused();
            }
        }

        //if (Input.GetKeyDown(KeyCode.X))
        //{

        //}

        if (nyawa < 1)
        {
            Kalah();
        }

    }

    public void Skill()
    {
        if (totalSkill > 0)
        {
            FindObjectOfType<SoundEffects>().SfxDuar3();
            FindObjectOfType<WordDisplay>().coba();
            //Destroy(GameObject.FindWithTag("kata"));
            totalSkill = Mathf.Clamp(totalSkill - 1, 0, 3);
        }
        else
        {
            Debug.Log("no sikil left");
        }
    }

    public void OnButtonPausePress()
    {
        if (isPauseGame)
        {
            Resume();
        }
        else
        {
            Paused();
        }
    }

    public void Resume()
    {
        
        PauseIcon.enabled = true;
        ResumeIcon.enabled = false;
        pauseScreen.SetActive(false);
        Time.timeScale = 1f;
        isPauseGame = false;
        //GameObject.FindGameObjectWithTag("ui2").SetActive(true);
    }

    public void Paused()
    {
        
        PauseIcon.enabled = false;
        ResumeIcon.enabled = true;
        pauseScreen.SetActive(true);
        Time.timeScale = 0f;
        isPauseGame = true;
        //GameObject.FindGameObjectWithTag("ui2").SetActive(false);
    }

    public void Kalah()
    {
        WordManager.isGameOver = true;
        //lose.Play();
        //gameObject.SetActive(false);
    }
}
