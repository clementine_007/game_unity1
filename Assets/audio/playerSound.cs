using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerSound : MonoBehaviour
{
    public AudioSource hit;

    void Start()
    {
        hit = GetComponent<AudioSource>();
    }

    void Update()
    {
  
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "kata")
        {
            hit.Play();

            //Destroy(other.gameObject);
        }
    }
}
