using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffects : MonoBehaviour
{
    //sound?
    public AudioSource bgmMusic;
    public AudioSource loseMusic;
    public AudioSource SfxSound1;
    public AudioSource SfxSound2;
    public AudioSource SfxSound3;

    public bool bgmSound = true;
    public bool loseSound = false;
    public bool sfxSound1 = false;
    public bool sfxSound2 = false;
    public bool sfxSound3 = false;

    public void BgmMusic()
    {
        bgmSound = true;
        loseSound = false;
        bgmMusic.Play();
    }

    public void SfxDuar()
    {
        sfxSound1 = true;
        SfxSound1.Play();
    }

    public void SfxDuar2()
    {
        sfxSound2 = true;
        SfxSound2.Play();
    }

    public void SfxDuar3()
    {
        sfxSound3 = true;
        SfxSound3.Play();
    }

    public void LoseMusic()
    {
        if (bgmMusic.isPlaying)
            bgmSound = false;
        {
            bgmMusic.Stop();
        }
        if (!loseMusic.isPlaying && loseSound == false)
        {
            loseMusic.Play();
            loseSound = true;
        }
    }

}
